package poker.version_graphics.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import poker.version_graphics.PokerGame;
import poker.version_graphics.model.Card;
import poker.version_graphics.model.DeckOfCards;
import poker.version_graphics.model.Player;
import poker.version_graphics.model.PokerGameModel;
import poker.version_graphics.view.ControlArea;
import poker.version_graphics.view.PlayerPane;
import poker.version_graphics.view.PokerGameView;

public class PokerGameController {
	private PokerGameModel model;
	private PokerGameView view;

	private int cardsRequired;
	private DeckOfCards deck;

	private int pointsOfPlayerOne, pointsOfPlayerTwo, pointsOfPlayerThree, pointsOfPlayerFour;

	public PokerGameController(PokerGameModel model, PokerGameView view) {
		this.model = model;
		this.view = view;

		// Values for the Results of each Player
		this.pointsOfPlayerOne = 1;
		this.pointsOfPlayerTwo = 1;
		this.pointsOfPlayerThree = 1;
		this.pointsOfPlayerFour = 1;

		view.getShuffleButton().setOnAction(e -> {
			try {
				shuffle();
			} catch (UnsupportedAudioFileException | IOException | LineUnavailableException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		view.getDealButton().setOnAction(e -> animationDeal());

	}

	private void animationDeal() {
		for (int i = 0; i < PokerGame.NUM_PLAYERS; i++) {
			Player p = model.getPlayer(i);
			p.discardHand();
			PlayerPane pp = view.getPlayerPane(i);
			pp.updatePlayerDisplay();
		}

		cardsRequired = PokerGame.NUM_PLAYERS * Player.HAND_SIZE;
		deck = model.getDeck();
		if (cardsRequired <= deck.getCardsRemaining()) {
			view.getControlArea().dishOut();
		} else {
			Alert alert = new Alert(AlertType.ERROR, "Not enough cards - shuffle first");
			alert.showAndWait();
		}

	}

	/**
	 * Remove all cards from players hands, and shuffle the deck
	 * 
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 * @throws LineUnavailableException
	 */
	private void shuffle() throws UnsupportedAudioFileException, IOException, LineUnavailableException {
		for (int i = 0; i < PokerGame.NUM_PLAYERS; i++) {
			Player p = model.getPlayer(i);
			p.discardHand();
			PlayerPane pp = view.getPlayerPane(i);
			pp.updatePlayerDisplay();
		}

		model.getDeck().shuffle();
		String soundName = "../poker_project/poker_project/src/Sounds/shuffleCardsSound.wav";
		AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(soundName).getAbsoluteFile());
		Clip clip = AudioSystem.getClip();
		clip.open(audioInputStream);
		clip.start();
	}

	/**
	 * Deal each player five cards, then evaluate the two hands
	 */
	public void deal() {

		cardsRequired = PokerGame.NUM_PLAYERS * Player.HAND_SIZE;
		deck = model.getDeck();
		
		for (int i = 0; i < PokerGame.NUM_PLAYERS; i++) {
			Player p = model.getPlayer(i);
			for (int j = 0; j < Player.HAND_SIZE; j++) {
				Card card = deck.dealCard();
				p.addCard(card);
			}
			p.evaluateHand();
			PlayerPane pp = view.getPlayerPane(i);
			pp.updatePlayerDisplay();
		}
		setTheWinnerLabelPoints();
	}

	public void setTheWinnerLabelPoints() {
		ArrayList<Player> winnerList = Player.evaluateWinner(model.getPlayerArray());

		for (int i = 0; i < winnerList.size(); i++) {
			if (winnerList.get(i).equals(model.getPlayerArray().get(0))) {
				view.getControlArea().setPlayerOneResultLabel(Integer.toString(this.pointsOfPlayerOne++));
			}

			if (winnerList.get(i).equals(model.getPlayerArray().get(1))) {
				view.getControlArea().setPlayerTwoResultLabel(Integer.toString(this.pointsOfPlayerTwo++));
			}

			if (PokerGame.NUM_PLAYERS == 3) {
				if (winnerList.get(i).equals(model.getPlayerArray().get(2))) {
					view.getControlArea().setPlayerThreeResultLabel(Integer.toString(this.pointsOfPlayerThree++));
				}
			}

			if (PokerGame.NUM_PLAYERS == 4) {
				if (winnerList.get(i).equals(model.getPlayerArray().get(2))) {
					view.getControlArea().setPlayerThreeResultLabel(Integer.toString(this.pointsOfPlayerThree++));
				}
				if (winnerList.get(i).equals(model.getPlayerArray().get(3))) {
					view.getControlArea().setPlayerFourResultLabel(Integer.toString(this.pointsOfPlayerFour++));
				}

			}

		}

	}

}
