package poker.version_graphics.model;

import java.util.ArrayList;
import java.util.Collections;

public class Player implements Comparable<Player> {
    public static final int HAND_SIZE = 5;
    
    private final String playerName; // This is the ID
    private final ArrayList<Card> cards = new ArrayList<>();
    private HandType handType;
    
    public Player(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return playerName;
    }

    public ArrayList<Card> getCards() {
        return cards;
    }
    
    public void addCard(Card card) {
        if (cards.size() < HAND_SIZE) cards.add(card);
    }
    
    public void discardHand() {
        cards.clear();
        handType = null;
    }
    
    public int getNumCards() {
        return cards.size();
    }

    /**
     * If the hand has not been evaluated, but does have all cards, 
     * then evaluate it.
     */
    public HandType evaluateHand() {
        if (handType == null && cards.size() == HAND_SIZE) {
            handType = HandType.evaluateHand(cards);
        }
        return handType;
    }
    
    /**
     * return an ArrayList with winners
     * 
     */
    public static ArrayList<Player> evaluateWinner(ArrayList<Player> players) {
    	ArrayList<Player> winnerList = new ArrayList<Player>();
    	ArrayList<Player> sortList = (ArrayList<Player>) players.clone();
    	Collections.sort(sortList);
    	for (int i = sortList.size() - 1; i >= 0; i--) {
    		if (winnerList.isEmpty() || winnerList.get(0).compareTo(sortList.get(i)) == 0) {
    			winnerList.add(sortList.get(i));
    		}
    	}
    	return evaluateTieBreak(winnerList);
    }
    
    /**
     * evaluate which HandType is higher
     * @param players
     * @return ArrayList
     */
    public static ArrayList<Player> evaluateTieBreak(ArrayList<Player> players){
    	ArrayList<Player> winnerList = players;
    	ArrayList<Player> finalWinners = new ArrayList<Player>();
    	
    	if(winnerList.size()==1) {
    		return winnerList;
    	}
    	
    	finalWinners.add(winnerList.get(winnerList.size()-1));
    	
    	for(int i = 0; i < winnerList.size() - 1; i++) {
    		if (winnerList.get(i).compareSameHandType(finalWinners.get(0)) > 0) {
    			finalWinners.clear();
    			finalWinners.add(winnerList.get(i));
    		} else {
    			if (winnerList.get(i).compareSameHandType(finalWinners.get(0)) == 0) {
    				finalWinners.add(winnerList.get(i));
    			}
    		}
    	}
    	   	return finalWinners;
     }
    	

    /**
     * Hands are compared, based on the evaluation they have.
     */
    @Override
    public int compareTo(Player o) {
        return handType.compareTo(o.handType);
    }
    
    /**
     * compare to of the same HandType
     * @param other
     * @return
     */
    public int compareSameHandType(Player other) {
    	HandType handType= this.evaluateHand();
    	//if the HaneType is a HighCard, Straight, Flush or a StraightFlush then it search 
    	//the highest card and compare it with the other player
    	if (handType.equals(HandType.HighCard) ||
    			handType.equals(HandType.Straight) ||
    			handType.equals(HandType.Flush) ||
    			handType.equals(HandType.StraightFlush)
    			) {
    		ArrayList<Card> p1cards = (ArrayList<Card>) this.cards.clone();
    		Collections.sort(p1cards);
    		ArrayList<Card> p2cards = (ArrayList<Card>) other.cards.clone();
    		Collections.sort(p2cards);
    		return p1cards.get(p1cards.size()-1).compareTo(p2cards.get(p2cards.size()-1));
    	}
    	
    	// search the highest card in the pair and compare it
    	if (handType.equals(HandType.OnePair)) {
    		Card cardTemp1 = null;
    		Card cardTemp2 = null;
    		boolean found = false; 
    		for (int i = 0; i < cards.size() - 1 && !found; i++) {
    	            for (int j = i+1; j < cards.size() && !found; j++) {
    	                if (cards.get(i).getRank() == cards.get(j).getRank()) {
    	                	cardTemp1 = cards.get(i);
    	                	found = true;
    	                }
    	            }
    		}
    		boolean found2 = false; 
    		for (int i = 0; i < other.cards.size() - 1 && !found2; i++) {
    	            for (int j = i+1; j < other.cards.size() && !found2; j++) {
    	                if (other.cards.get(i).getRank() == other.cards.get(j).getRank()) {
    	                	cardTemp2 = other.cards.get(i);
    	                	found2 = true;
    	                }
    	            }
    		}
    		return cardTemp1.compareTo(cardTemp2);	
    	}
    	
    	// search the highest card from the two pairs and compare it
    	if (handType.equals(HandType.TwoPair)) {
    		Card p1c1 = null;
    		Card p1c2 = null;
    		Card p2c1 = null;
    		Card p2c2 = null;
    		Card card1 = null;
    		Card card2 = null;
    		
    	        // Clone the cards, because we will be altering the list
    	       ArrayList<Card> clonedCards = (ArrayList<Card>) cards.clone();

    	        // Find the first pair; if found, remove the cards from the list
    	        boolean firstPairFound = false;
    	        for (int i = 0; i < clonedCards.size() - 1 && !firstPairFound; i++) {
    	            for (int j = i+1; j < clonedCards.size() && !firstPairFound; j++) {
    	                if (clonedCards.get(i).getRank() == clonedCards.get(j).getRank()) {
    	                    firstPairFound = true;
    	                    p1c1 = clonedCards.get(j);
    	                    clonedCards.remove(j);  // Remove the later card
    	                    clonedCards.remove(i);  // Before the earlier one
    	                }
    	            }
    	        }
    	        boolean found = false; 
        		for (int i = 0; i < clonedCards.size() - 1 && !found; i++) {
        	            for (int j = i+1; j < clonedCards.size() && !found; j++) {
        	                if (clonedCards.get(i).getRank() == clonedCards.get(j).getRank()) {
        	                	p1c2 = clonedCards.get(i);
        	                	found = true;
        	                }
        	            }
        		}
    	    
        		  ArrayList<Card> clonedCards2 = (ArrayList<Card>) other.cards.clone();

      	        // Find the first pair; if found, remove the cards from the list
      	        boolean firstPairFound2 = false;
      	        for (int i = 0; i < clonedCards2.size() - 1 && !firstPairFound2; i++) {
      	            for (int j = i+1; j < clonedCards2.size() && !firstPairFound2; j++) {
      	                if (clonedCards2.get(i).getRank() == clonedCards2.get(j).getRank()) {
      	                	firstPairFound2 = true;
      	                    p2c1 = clonedCards2.get(j);
      	                  clonedCards2.remove(j);  // Remove the later card
      	                clonedCards2.remove(i);  // Before the earlier one
      	                }
      	            }
      	        }
      	        boolean found2 = false; 
          		for (int i = 0; i < clonedCards2.size() - 1 && !found2; i++) {
          	            for (int j = i+1; j < clonedCards2.size() && !found2; j++) {
          	                if (clonedCards2.get(i).getRank() == clonedCards2.get(j).getRank()) {
          	                	p2c2 = clonedCards2.get(i);
          	                	found2 = true;
          	                }
          	            }
          		}
          		if (p1c1.compareTo(p1c2) > 0 )
          			card1 = p1c1;
          		else
          			card1 = p1c2;
          		
          		if (p2c1.compareTo(p2c2) > 0 )
          			card2 = p2c1;
          		else
          			card2 = p2c2;
        		
          		return card1.compareTo(card2);
    	}
    	
    	// search the highest card in the ThreeOfAKind and compare it
    	if (handType.equals(HandType.ThreeOfAKind) ||
    			handType.equals(HandType.FullHouse)
    			) {
    		Card card1 = null;
    		Card card2 = null;
    		boolean found = false;
            for (int i = 0; i < cards.size() - 2 && !found; i++) {
                for (int j = i+1; j < cards.size() - 1 && !found; j++) {
                	 for (int k = j+1; k < cards.size() && !found; k++) {
                		 
                		 if ((cards.get(i).getRank() == cards.get(j).getRank()) 
                				 && (cards.get(i).getRank() == cards.get(k).getRank())) {
                			 card1 = cards.get(i);
                			 found = true;
                		 } 
                	 }
                }
            }
            boolean found2 = false;
            for (int i = 0; i < other.cards.size() - 2 && !found2; i++) {
                for (int j = i+1; j < other.cards.size() - 1 && !found2; j++) {
                	 for (int k = j+1; k < other.cards.size() && !found2; k++) {
                		 
                		 if ((other.cards.get(i).getRank() == other.cards.get(j).getRank()) 
                				 && (other.cards.get(i).getRank() == other.cards.get(k).getRank())) {
                			 card2 = other.cards.get(i);
                			 found2 = true;
                		 } 
                	 }
                }
            }
            return card1.compareTo(card2);
    	}
    	
    	// search the highest card in the FourOfAKind and compare it
    	if (handType.equals(HandType.FourOfAKind)) {
    		Card card1 = null;
    		Card card2 = null;
    		
    		boolean found = false;
            for (int i = 0; i < cards.size() - 2 && !found; i++) {
                for (int j = i+1; j < cards.size() - 1 && !found; j++) {
                	 for (int k = j+1; k < cards.size() && !found; k++) {
                		 for (int h = k+1; h < cards.size() && !found; h++) {
                			 
                			 if ((cards.get(i).getRank() == cards.get(j).getRank()) 
                    				 && (cards.get(j).getRank() == cards.get(k).getRank())
                    				 && (cards.get(k).getRank() == cards.get(h).getRank())) {
                				 card1 = cards.get(i);
                				 found = true;
                			 }
                		 }
                	 }
                }
            }
            boolean found2 = false;
            for (int i = 0; i < other.cards.size() - 2 && !found2; i++) {
                for (int j = i+1; j < other.cards.size() - 1 && !found2; j++) {
                	 for (int k = j+1; k < other.cards.size() && !found2; k++) {
                		 for (int h = k+1; h < other.cards.size() && !found2; h++) {
                			 
                			 if ((other.cards.get(i).getRank() == other.cards.get(j).getRank()) 
                    				 && (other.cards.get(j).getRank() == other.cards.get(k).getRank())
                    				 && (other.cards.get(k).getRank() == other.cards.get(h).getRank())) {
                				 card2 = other.cards.get(i);
                				 found2 = true;
                			 }
                		 }
                	 }
                }
            }
            return card1.compareTo(card2);
    	}
    	else
    		return 0;
	}
        
}


