package poker.version_graphics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import poker.version_graphics.view.ControlArea;

 
public class Initializer {
	
	public static int readeFile()  {
		Scanner fileScan;
		 int num = 0;
		 String url = "../poker_project/poker_project/src/poker/version_graphics/finalFile.txt";
		
		try {
		
			fileScan = new Scanner(new File(url));
			while(fileScan.hasNext()) {
				num = Integer.parseInt(fileScan.next());
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return num;
	}

	public static void writeFile() {
		try {
			String url = "../poker_project/poker_project/src/poker/version_graphics/finalFile.txt";
			File file = new File(url); //change path
	    
			if(!file.exists()) {
					file.createNewFile();	
			}
			
			PrintWriter pw = new PrintWriter(file);
			
			pw.println(Integer.toString(ControlArea.getSelectedItem()));
			pw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
 
}