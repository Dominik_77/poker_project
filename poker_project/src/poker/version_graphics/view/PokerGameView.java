package poker.version_graphics.view;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import poker.version_graphics.Initializer;
import poker.version_graphics.PokerGame;
import poker.version_graphics.controller.PokerGameController;
import poker.version_graphics.model.PokerGameModel;


public class PokerGameView {
	private GridPane splitGrid;
	private ControlArea controls;
	private PokerGameController controller;
	private PokerGameModel model;
	private BorderPane root;
	
	public PokerGameView(Stage stage, PokerGameModel model) {
		this.model = model;
			
		// Create the GridPane to put the HBox Players in it
		splitGrid = new GridPane();

		// creates PlayerPane
		int num = PokerGame.NUM_PLAYERS;
		int grid1 = 0;
		int grid2 = 0;
		
		for (int i = 0; i < num; i++) {
			PlayerPane player = new PlayerPane();
			player.setPlayer(model.getPlayer(i));
			if (i == 0) {
				grid1 = 1;
				grid2 = 1;
			}
			if (i == 1) {
				grid1 = 2;
				grid2 = 1;
			}
			if (i == 2) {
				grid1 = 1;
				grid2 = 2;
			}
			if (i == 3) {
				grid1 = 2;
				grid2 = 2;
			}
			splitGrid.add(player, grid1, grid2);
		}

		// Create the control area
		controls = new ControlArea();
		controls.linkDeck(model.getDeck()); // link DeckLabel to DeckOfCards in the logic

		// Put players and controls into a BorderPane
		this.root = new BorderPane();
		root.setCenter(splitGrid);
		root.setBottom(controls);

		controls.getBox().getSelectionModel().selectedIndexProperty()
		.addListener((observable, oldValue, newValue) -> {
			  //save number of players
			  Initializer.writeFile();
			  
			  stage.close();
			  Platform.runLater( () -> {
				try {
					new PokerGame().start( new Stage() );  //restart
				} catch (Exception e) {
					e.printStackTrace();
				}
			} );
			} );
		// Disallow resizing - which is difficult to get right with images
		stage.setResizable(false);

		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int height = (int)screenSize.getHeight();
		Scene scene = new Scene(root, 1190, 780);
		scene.getStylesheets().add(getClass().getResource("poker.css").toExternalForm());
		stage.setTitle("Poker Miniproject");
		stage.setScene(scene);
		stage.show();
		
		
		Image image = new Image(this.getClass().getClassLoader().getResourceAsStream("poker/images/icon.png"));
		stage.getIcons().add(image);
		
		//Sets the ID for the root
		this.root.setId("BorderPane");
	}

	public PlayerPane getPlayerPane(int i) {
		return (PlayerPane) splitGrid.getChildren().get(i);
	}

	public Button getShuffleButton() {
		return controls.btnShuffle;
	}

	public Button getDealButton() {
		return controls.btnDeal;
	}

	public ControlArea getControlArea() {
		return controls;
	}
	public void setPokerGameController(PokerGameController controller) {
		this.controller = controller;
		controls.setPokerGameController(controller);
	}
}
