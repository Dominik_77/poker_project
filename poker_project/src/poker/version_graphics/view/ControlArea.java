package poker.version_graphics.view;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javafx.animation.ParallelTransition;
import javafx.animation.PathTransition;
import javafx.animation.PauseTransition;
import javafx.animation.SequentialTransition;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.PathElement;
import javafx.util.Duration;
import poker.version_graphics.PokerGame;
import poker.version_graphics.controller.PokerGameController;
import poker.version_graphics.model.DeckOfCards;

public class ControlArea extends HBox {
	private PokerGameController controller;
	private Label selectNumberlbl;
	private DeckLabel lblDeck = new DeckLabel();
	private Region spacer = new Region(); // Empty spacer
	private Region spacer2 = new Region();//Empty spacer
	private Region spacer3 = new Region();//Empty spacer
	Button btnShuffle = new Button("Shuffle");
	Button btnDeal = new Button("Deal");
	Image deck;
	Label deckLabelImage;
	private StackPane stackpane = new StackPane();
	private int cardH = 162;
	private int cardW = 108;
	private double dishOutTime = 0.1;
	private SequentialTransition seqT;
	
	
	//All the Elements for the ResultBox
	private VBox resultBox;
	private Label resultText;
	private Label player1,player2,player3,player4;
	private Label player1LabelResult,player2LabelResult,player3LabelResult,player4LabelResult;
	private HBox player1HBox,player2HBox,player3HBox,player4HBox;

	private static ChoiceBox<Integer> box;

	public ControlArea()  {
		super(); // Always call super-constructor first !!
		
		//Sets the back Image of the Deck as a Label in the HBox
		this.deckLabelImage= new Label();
		this.deck=new Image(this.getClass().getClassLoader().getResourceAsStream("poker/images/" + "back.png"));
		ImageView imv = new ImageView(deck);
		imv.setFitHeight(cardH);
		imv.setFitWidth(cardW);
		this.deckLabelImage.setGraphic(imv);
		
		this.stackpane.getChildren().add(deckLabelImage);
		
		this.selectNumberlbl=new Label("Multiplayer");
		
		box = new ChoiceBox<Integer>();
		box.getItems().add(2);
		box.getItems().add(3);
		box.getItems().add(4);
		
		box.setValue(PokerGame.NUM_PLAYERS);
		
		//Setting the ID for the Controlllabels
		this.selectNumberlbl.setId("selectNumberlbl");
		this.lblDeck.setId("lblDeck");
		this.lblDeck.setPrefWidth(80);
		this.btnShuffle.setId("btnShuffle");
		this.btnDeal.setId("btnDeal");
		box.setId("choiceBox");
		this.spacer.setId("spacer");
		this.spacer2.setId("spacer2");
		this.spacer3.setId("spacer3");
	
		this.setId("controlArea"); // Unique ID in the CSS
		
		
		//Create the ResultBox
				this.resultBox= new VBox();
				this.resultBox.setId("resultBox");
				this.resultText = new Label("Results");
				this.resultText.setId("resultText");
				this.player1= new Label("Player1");
				this.player1.setId("player1");
				this.player2= new Label("Player2");
				this.player2.setId("player2");
				this.player3= new Label("Player3");
				this.player3.setId("player3");
				this.player4= new Label("Player4");
				this.player4.setId("player4");
				
				this.player1LabelResult= new Label("---");
				this.player2LabelResult= new Label("---");
				this.player3LabelResult= new Label("---");
				this.player4LabelResult= new Label("---");
				this.player1LabelResult.setId("player1LabelResult");
				this.player2LabelResult.setId("player2LabelResult");
				this.player3LabelResult.setId("player3LabelResult");
				this.player4LabelResult.setId("player4LabelResult");
				
				this.player1HBox= new HBox();
				this.player1HBox.getChildren().addAll(this.player1,this.player1LabelResult);
				
				this.player2HBox= new HBox();
				this.player2HBox.getChildren().addAll(this.player2,this.player2LabelResult);
				
				this.player3HBox= new HBox();
				this.player3HBox.getChildren().addAll(this.player3,this.player3LabelResult);
				
				this.player4HBox= new HBox();
				this.player4HBox.getChildren().addAll(this.player4,this.player4LabelResult);
				
				this.resultBox.getChildren().addAll(this.resultText,this.player1HBox,this.player2HBox,this.player3HBox,this.player4HBox);
				
				//sets the elements in the HBox
				this.getChildren().addAll(stackpane,lblDeck,spacer,resultBox,spacer2,selectNumberlbl, box,spacer3, btnShuffle, btnDeal);
	}
	
	//Getters to set the Points for the Winners in the deal Function
	public Label getPlayerOneResultLabel() {
		return this.player1LabelResult;
	}
	public Label getPlayerTwoResultLabel() {
		return this.player2LabelResult;
	}
	public Label getPlayerThreeResultLabel() {
		return this.player3LabelResult;
	}
	public Label getPlayerFourResultLabel() {
		return this.player4LabelResult;
	}
	
	public void setPlayerOneResultLabel(String points) {
		this.player1LabelResult.setText(points);
	}
	
	public void setPlayerTwoResultLabel(String points) {
		this.player2LabelResult.setText(points);
	}
	
	public void setPlayerThreeResultLabel(String points) {
		this.player3LabelResult.setText(points);
	}
	
	public void setPlayerFourResultLabel(String points) {
		this.player4LabelResult.setText(points);
	}
	
	public ChoiceBox<Integer> getBox(){
		return box;
	}

	public void linkDeck(DeckOfCards deck) {
		lblDeck.setDeck(deck);
	}

	public static int getSelectedItem() {
		return box.getSelectionModel().getSelectedIndex()+2;
	}
	
public void dishOut() {
		
		ArrayList<Label> labels = new ArrayList<Label>();
		
		//create card labels
		for (int i = 0; i < 20; i++) {
			Label lblCard= new Label();
			Image deck=new Image(this.getClass().getClassLoader().getResourceAsStream
					("poker/images/" + "back.png"));
			ImageView imv = new ImageView(deck);
			imv.setFitHeight(cardH);
			imv.setFitWidth(cardW);
			lblCard.setGraphic(imv);
			lblCard.setId("deckLabelImage");
			labels.add(lblCard);
		}
		
		//add card labels to the stackpane
		for (int i = 0; i < 20; i++) {
			stackpane.getChildren().addAll(labels.get(i));
		}
		
		//create a path for each card
		PathElement pe0 = new MoveTo(0, 0);
		PathElement pe1 = new LineTo(67, -451);
		PathElement pe2 = new LineTo(178, -451);
		PathElement pe3 = new LineTo(289, -451);
		PathElement pe4 = new LineTo(400, -451);
		PathElement pe5 = new LineTo(512, -451);
		PathElement pe6 = new LineTo(663, -451);
		PathElement pe7 = new LineTo(774, -451);
		PathElement pe8 = new LineTo(885, -451);
		PathElement pe9 = new LineTo(996, -451);
		PathElement pe10 = new LineTo(1108, -451);
		
		PathElement pe11 = new LineTo(67, -155);
		PathElement pe12 = new LineTo(178, -155);
		PathElement pe13 = new LineTo(289, -155);
		PathElement pe14 = new LineTo(400, -155);
		PathElement pe15 = new LineTo(512, -155);
		PathElement pe16 = new LineTo(663, -155);
		PathElement pe17 = new LineTo(774, -155);
		PathElement pe18 = new LineTo(885, -155);
		PathElement pe19 = new LineTo(996, -155);
		PathElement pe20 = new LineTo(1108, -155);
		
		//back to stack
		PathElement pe100 = new LineTo(0, 0);
		
		
		Path path1 = new Path();
		path1.getElements().add(pe0);
		path1.getElements().add(pe1);
		
		Path path2 = new Path();
		path2.getElements().add(pe0);
		path2.getElements().add(pe2);
		
		Path path3 = new Path();
		path3.getElements().add(pe0);
		path3.getElements().add(pe3);
		
		Path path4 = new Path();
		path4.getElements().add(pe0);
		path4.getElements().add(pe4);
		
		Path path5 = new Path();
		path5.getElements().add(pe0);
		path5.getElements().add(pe5);
		
		Path path6 = new Path();
		path6.getElements().add(pe0);
		path6.getElements().add(pe6);
		
		Path path7 = new Path();
		path7.getElements().add(pe0);
		path7.getElements().add(pe7);
		
		Path path8 = new Path();
		path8.getElements().add(pe0);
		path8.getElements().add(pe8);
		
		Path path9 = new Path();
		path9.getElements().add(pe0);
		path9.getElements().add(pe9);
		
		Path path10 = new Path();
		path10.getElements().add(pe0);
		path10.getElements().add(pe10);
		
		Path path11 = new Path();
		path11.getElements().add(pe0);
		path11.getElements().add(pe11);
		
		Path path12 = new Path();
		path12.getElements().add(pe0);
		path12.getElements().add(pe12);
		
		Path path13 = new Path();
		path13.getElements().add(pe0);
		path13.getElements().add(pe13);
		
		Path path14 = new Path();
		path14.getElements().add(pe0);
		path14.getElements().add(pe14);
		
		Path path15 = new Path();
		path15.getElements().add(pe0);
		path15.getElements().add(pe15);
		
		Path path16 = new Path();
		path16.getElements().add(pe0);
		path16.getElements().add(pe16);
		
		Path path17 = new Path();
		path17.getElements().add(pe0);
		path17.getElements().add(pe17);
		
		Path path18 = new Path();
		path18.getElements().add(pe0);
		path18.getElements().add(pe18);
		
		Path path19 = new Path();
		path19.getElements().add(pe0);
		path19.getElements().add(pe19);
		
		Path path20 = new Path();
		path20.getElements().add(pe0);
		path20.getElements().add(pe20);
		
		//Carts invisible
		Path path100 = new Path();
		path100.getElements().add(pe0);
		path100.getElements().add(pe100);
		
		//give each card the path
		PathTransition move1 = new PathTransition(Duration.seconds(dishOutTime), path1, labels.get(0));
		PathTransition move2 = new PathTransition(Duration.seconds(dishOutTime), path2, labels.get(1));
		PathTransition move3 = new PathTransition(Duration.seconds(dishOutTime), path3, labels.get(2));
		PathTransition move4 = new PathTransition(Duration.seconds(dishOutTime), path4, labels.get(3));
		PathTransition move5 = new PathTransition(Duration.seconds(dishOutTime), path5, labels.get(4));
		
		PathTransition move6 = new PathTransition(Duration.seconds(dishOutTime), path6, labels.get(5));
		PathTransition move7 = new PathTransition(Duration.seconds(dishOutTime), path7, labels.get(6));
		PathTransition move8 = new PathTransition(Duration.seconds(dishOutTime), path8, labels.get(7));
		PathTransition move9 = new PathTransition(Duration.seconds(dishOutTime), path9, labels.get(8));
		PathTransition move10 = new PathTransition(Duration.seconds(dishOutTime), path10, labels.get(9));
		
		PathTransition move11 = new PathTransition(Duration.seconds(dishOutTime), path11, labels.get(10));
		PathTransition move12 = new PathTransition(Duration.seconds(dishOutTime), path12, labels.get(11));
		PathTransition move13 = new PathTransition(Duration.seconds(dishOutTime), path13, labels.get(12));
		PathTransition move14 = new PathTransition(Duration.seconds(dishOutTime), path14, labels.get(13));
		PathTransition move15 = new PathTransition(Duration.seconds(dishOutTime), path15, labels.get(14));
		
		PathTransition move16 = new PathTransition(Duration.seconds(dishOutTime), path16, labels.get(15));
		PathTransition move17 = new PathTransition(Duration.seconds(dishOutTime), path17, labels.get(16));
		PathTransition move18 = new PathTransition(Duration.seconds(dishOutTime), path18, labels.get(17));
		PathTransition move19 = new PathTransition(Duration.seconds(dishOutTime), path19, labels.get(18));
		PathTransition move20 = new PathTransition(Duration.seconds(dishOutTime), path20, labels.get(19));
		
		PathTransition move100 = new PathTransition(Duration.seconds(0.1), path100, labels.get(0));
		PathTransition move101 = new PathTransition(Duration.seconds(0.1), path100, labels.get(1));
		PathTransition move102 = new PathTransition(Duration.seconds(0.1), path100, labels.get(2));
		PathTransition move103 = new PathTransition(Duration.seconds(0.1), path100, labels.get(3));
		PathTransition move104 = new PathTransition(Duration.seconds(0.1), path100, labels.get(4));
		PathTransition move105 = new PathTransition(Duration.seconds(0.1), path100, labels.get(5));
		PathTransition move106 = new PathTransition(Duration.seconds(0.1), path100, labels.get(6));
		PathTransition move107 = new PathTransition(Duration.seconds(0.1), path100, labels.get(7));
		PathTransition move108 = new PathTransition(Duration.seconds(0.1), path100, labels.get(8));
		PathTransition move109 = new PathTransition(Duration.seconds(0.1), path100, labels.get(9));
		PathTransition move110 = new PathTransition(Duration.seconds(0.1), path100, labels.get(10));
		PathTransition move111 = new PathTransition(Duration.seconds(0.1), path100, labels.get(11));
		PathTransition move112 = new PathTransition(Duration.seconds(0.1), path100, labels.get(12));
		PathTransition move113 = new PathTransition(Duration.seconds(0.1), path100, labels.get(13));
		PathTransition move114 = new PathTransition(Duration.seconds(0.1), path100, labels.get(14));
		PathTransition move115 = new PathTransition(Duration.seconds(0.1), path100, labels.get(15));
		PathTransition move116 = new PathTransition(Duration.seconds(0.1), path100, labels.get(16));
		PathTransition move117 = new PathTransition(Duration.seconds(0.1), path100, labels.get(17));
		PathTransition move118 = new PathTransition(Duration.seconds(0.1), path100, labels.get(18));
		PathTransition move119 = new PathTransition(Duration.seconds(0.1), path100, labels.get(19));
		
		ParallelTransition parallel = new ParallelTransition(move100, move101,move102, move103, 
				move104, move105, move106, move107, move108, move109, move110, move111, move112, 
				move113, move114, move115, move116, move117, move118, move119);
		
		SequentialTransition seqT3 = null;
		
		switch (PokerGame.NUM_PLAYERS) {
		case (2): 
			seqT3 = new SequentialTransition(move1, move2, move3, move4, move5, move6, move7, move8, move9, 
				move10);
		break;
		case (3):  
			seqT3 = new SequentialTransition(move1, move2, move3, move4, move5, move6, move7, move8, move9, 
				move10, move11, move12, move13, move14, move15); 
		break;
		case (4):  
			seqT3 = new SequentialTransition(move1, move2, move3, move4, move5, move6, move7, move8, move9, 
					move10, move11, move12, move13, move14, move15, move16, move17, move18, move19, move20); 
		break;
		}
		
		PauseTransition pause = new PauseTransition(Duration.millis(500));
		seqT = new SequentialTransition (seqT3, pause, parallel);
		
		seqT.setCycleCount(1);
		seqT.play();
		
		seqT.setOnFinished(e -> controller.deal());

	}
	
	public void setPokerGameController(PokerGameController controller) {
		this.controller = controller;
	}
	
}
